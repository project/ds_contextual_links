<?php
/**
 * @file
 *   Main module code for the "Display Suite Contextual Links" module.
 *
 *   © 2020 Inveniem. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@inveniem.com)
 */
require_once('ds_contextual_links.constants.inc');

//==============================================================================
// Hook Implementations
//==============================================================================
/**
 * Implements hook_module_implements_alter().
 */
function ds_contextual_links_module_implements_alter(array &$implementations,
                                                     string $hook) {
  // Invoke this module last, to ensure that the "additional settings" vertical
  // tab/fieldset has been populated for us before we add to it.
  if ($hook == 'form_alter' && isset($implementations[DSCOLI_MODULE_NAME])) {
    $group = $implementations[DSCOLI_MODULE_NAME];
    unset($implementations[DSCOLI_MODULE_NAME]);
    $implementations[DSCOLI_MODULE_NAME] = $group;
  }
}

/**
 * Implements hook_entity_view_alter().
 *
 * Removes contextual links for view modes that have contextual links disabled.
 */
function ds_contextual_links_entity_view_alter(array &$build,
                                               string $entity_type) {
  if (!isset($build['#entity_type']) || !isset($build['#bundle'])) {
    return;
  }

  $bundle     = $build['#bundle'];
  $view_mode  = $build['#view_mode'];
  $layout     = ds_get_layout($entity_type, $bundle, $view_mode);

  if (!_ds_contextual_links_should_show_links($layout['settings'])) {
    unset($build['#contextual_links']);
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for the field_ui_display_overview_form.
 *
 * We provide the option to toggle-off contextual links on custom displays. The
 * default display does not have this option because contextual links aren't
 * displayed on the default display. The "Tabs No More" module moves entity tabs
 * into contextual links, but that module works differently than normal entity
 * contextual links.
 */
function ds_contextual_links_form_field_ui_display_overview_form_alter(
                                            array &$form, array &$form_state) {
  $view_mode = $form['#view_mode'] ?? DSCOLI_VIEW_MODE_DEFAULT;
  $ds_layout = $form['#ds_layout'] ?? NULL;

  if (($ds_layout === NULL) || ($view_mode === DSCOLI_VIEW_MODE_DEFAULT)) {
    return;
  }

  $ds_settings  = $ds_layout->settings ?? array();
  $show_links   = _ds_contextual_links_should_show_links($ds_settings);

  $fieldset =& $form['additional_settings'][DSCOLI_FIELDSET_CONTEXTUAL_LINKS];

  $fieldset = array(
    '#type'   => 'fieldset',
    '#title'  => t('Contextual links'),
  );

  $fieldset[DSCOLI_ELEMENT_SHOW_CONTEXTUAL_LINKS] = array(
    '#title'          => t('Enable contextual links in this view mode'),
    '#type'           => 'checkbox',
    '#default_value'  => $show_links,
  );
}

/**
 * Implements hook_ds_layout_settings_alter().
 *
 * This saves the custom setting for enabling/disabling contextual links.
 */
function ds_contextual_links_ds_layout_settings_alter($record,
                                                      array $form_state) {
  $additional_settings =
    $form_state['values']['additional_settings'] ?? array();

  $fieldset = $additional_settings[DSCOLI_FIELDSET_CONTEXTUAL_LINKS] ?? array();

  $show_links = $fieldset[DSCOLI_ELEMENT_SHOW_CONTEXTUAL_LINKS] ?? TRUE;

  $record->settings[DSCOLI_SETTING_SHOW_CONTEXTUAL_LINKS] = (bool)$show_links;
}

//==============================================================================
// Internal API
//==============================================================================
/**
 * Returns whether to show contextual links based on DS settings.
 *
 * @param array $ds_settings
 *   The Display Suite settings for the current view mode.
 *
 * @return bool
 *   TRUE (the default) if the view mode settings indicate contextual links
 *   should be shown; or, FALSE otherwise.
 */
function _ds_contextual_links_should_show_links(array $ds_settings): bool {
  return $ds_settings[DSCOLI_SETTING_SHOW_CONTEXTUAL_LINKS] ?? TRUE;
}
