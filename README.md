# Display Suite Contextual Links

## How to Use
1. Install [Display Suite](https://drupal.org/project/ds).
2. Install this module.
3. Enable both modules.
4. Open the "Manage display" tab for the target entity type.
5. Under "Custom display settings", enable the view modes that you want to
   customize and save the form.
6. At the top of the page, select the view mode that you'd like to customize 
   (e.g. "Teaser").
7. At the bottom of the page, under the "Layout for ENTITY TYPE in VIEW MODE" 
   vertical tab, select a Display Suite layout ("One Column" tends to work for
   common cases) and save the form.
8. At the bottom of the form, under the "Contextual links" vertical tab,
   uncheck "Enable contextual links in this view mode" if you want to hide
   contextual links in the view mode.

All view modes already default to displaying contextual links, so this module
is primarily used for hiding contextual links when they are not desired.
