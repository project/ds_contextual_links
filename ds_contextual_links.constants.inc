<?php
/**
 * @file
 *   Constants for the "Display Suite Contextual Links" module.
 *
 *   © 2020 Inveniem. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@inveniem.com)
 */

/**
 * The machine name for this module.
 */
define('DSCOLI_MODULE_NAME', 'ds_contextual_links');

/**
 * The machine name of the default view mode.
 */
define('DSCOLI_VIEW_MODE_DEFAULT', 'default');

/**
 * The name of the setting that controls the appearance of contextual links.
 */
define('DSCOLI_SETTING_SHOW_CONTEXTUAL_LINKS', 'show_contextual_links');

/**
 * The machine name of the fieldset for contextual links settings.
 */
define('DSCOLI_FIELDSET_CONTEXTUAL_LINKS', 'contextual_links');

/**
 * The machine name of the fieldset for contextual links settings.
 */
define('DSCOLI_ELEMENT_SHOW_CONTEXTUAL_LINKS', 'show');
